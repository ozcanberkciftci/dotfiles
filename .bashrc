# ~/.bashrc
# If not running interactively, don't do anything
[[ $- != *i* ]] && return
# Prompt customization
PS1='\e[0;34m[\u@\h [\w] '
# Display system information
ufetch
